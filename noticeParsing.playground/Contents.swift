//: Playground - noun: a place where people can play

import Foundation

let 가정통신문 : String = "http://dsm.hs.kr/school_3.brd?shell=/index.shell:209"
let 공지사항 : String = "http://dsm.hs.kr/notice.brd?shell=/index.shell:210"
struct Article {
    let title : String, desc : String, writer : String, time : String
    
    init(title : String, desc : String, writer : String, time : String){
        self.title = title
        self.desc = desc
        self.writer = writer
        self.time = time
    }
}



class getArticle{
    private let DEBUG : Bool = true
    private var pageUrl : String?
    private var html : String?
    
    init(url : String){
        self.pageUrl = url
        getHtmlSource()
    }
    
    func getArticles() -> Array<Article> {
        var result : Array<Article> = Array<Article>()
        let data = html?.components(separatedBy: "Posts=")[1].components(separatedBy: "_post_list(Posts);")[0]
        var stack : UInt8 = 0
        for temp in (data?.components(separatedBy: "[[["))! {
            stack += 1
            if(stack < 3){
                continue
            }
            
            let desc = temp.components(separatedBy: "\"")[9] == "\\n" ? "" : temp.components(separatedBy: "\"")[9]
            let time = "\(temp.components(separatedBy: "\"")[15])\(temp.components(separatedBy: "\"")[16])\(temp.components(separatedBy: "\"")[17])"
            let fileCount = ((temp.components(separatedBy: "[").count / 3) - 1) * 14
            let writer : String = temp.components(separatedBy: "\"")[19 + fileCount]
        
            let title = temp.components(separatedBy: "\"")[21 + fileCount]
            result.append(Article(title : title, desc : desc, writer : writer, time : time))
            if(DEBUG){
                print("TIME : ", time)
                print("TITLE : ", title)
                print("DESC : ", desc)
                print("Writer : ", writer)
                print("[ STACK : ", temp.components(separatedBy: "[").count)
                print("\n\n")
            }
        }
        return result
    }
    
    private func getHtmlSource() {
        if let url = URL(string : self.pageUrl!){
            do{
                self.html = try String(contentsOf : url as URL)
            }catch{
                print(error)
            }
        }
    }
}

let t = getArticle(url : 가정통신문)
let a : Array<Article> = t.getArticles()
for d in a {
    print("----------------------------------------")
    print("TITLE : ", d.title)
    print("TIME : ", d.time)
    print("Writer : ", d.writer)
    print("DESC : ", d.desc)
    print("----------------------------------------\n\n")
}